package br.com.senac.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView txtResultado;
    private Button btnSete;
    private Button btnOito;
    private Button btnNove;
    private Button btnDividir;
    private Button btnQuatro;
    private Button btnCinco;
    private Button btnSeis;
    private Button btnMultiplicar;
    private Button btnUm;
    private Button btnDois;
    private Button btnTres;
    private Button btnSubtrair;
    private Button btnPonto;
    private Button btnZero;
    private Button btnLimpar;
    private Button btnSomar;
    private Button btnIgual;
    private Boolean limpaResultado = true;
    private Boolean limpaDisplay = true;

    public void init(){
        this.txtResultado = findViewById(R.id.Resultado);
        this.btnSete = findViewById(R.id.btnSete);
        this.btnOito = findViewById(R.id.btnOito);
        this.btnNove = findViewById(R.id.btnNove);
        this.btnDividir = findViewById(R.id.btnDividir);
        this.btnQuatro = findViewById(R.id.btnQuatro);
        this.btnCinco = findViewById(R.id.btnCinco);
        this.btnSeis = findViewById(R.id.btnSeis);
        this.btnMultiplicar = findViewById(R.id.btnMultiplicar);
        this.btnUm = findViewById(R.id.btnUm);
        this.btnDois = findViewById(R.id.btnDois);
        this.btnTres = findViewById(R.id.btnTres);
        this.btnSubtrair = findViewById(R.id.btnSubtrair);
        this.btnPonto = findViewById(R.id.btnPonto);
        this.btnZero = findViewById(R.id.btnZero);
        this.btnLimpar = findViewById(R.id.btnLimpar);
        this.btnSomar = findViewById(R.id.btnSomar);
        this.btnIgual = findViewById(R.id.btnIgual);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.txtResultado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button button = (Button) view;
                String textoDoBotao = button.getText().toString();

                if(limpaResultado){
                    txtResultado.setText(textoDoBotao);
                    limpaDisplay = false;

                }else{
                    txtResultado.setText(txtResultado.getText()+textoDoBotao);
                }
            }
        });


    }
}
